## Adicionando o seu site

Para adicionar um novo site à liga, por favor inclua as seguintes informações:

- Nome do seu site
- URL do seu site
- URL do seu feed RSS (opcional)

Exemplo:

```json
{
  "title": "Título do Site",
  "url": "https://www.url-do-site.org",
  "feed": null
}
```
