# Liga de Blogs - Kit Inicial

Um kit para hospedar a sua própria comunidade de sites.

Usa o [Eleventy](https://www.11ty.io) e o [Netlify](https://www.netlify.com/) para construir um diretório central para sites membros. Pessoas podem usar os links `/prev`, `/random` and `/next` para serem redirecionados para os membros da liga.

## Funções

- Administradores gerenciam a liga no GitLab
- Membros são definidos no `src/data/members.json`
- Permite que pessoas adicionem seus sites via _merge request_ ou enviem através de um formulário de email
- Publica um código de conduta
- Provê um código incorporável que renderiza um banner (como um web component)
- Publica um índice dos feeds RSS de todos os membros
- Mostra um mapa em SVG do anel e de seus membros

## Exemplo

Veja o **[Site de demonstração](https://webringdemo.netlify.com)** gerado por este código.

## Como hospedar uma liga

1. Faça um _fork_ deste repositório
2. Edite o `src/data/meta.json` e o preencha com informações da sua comunidade
3. Adicione uma imagem de avatar para a sua liga em `src/assets/images`
4. Remova os membros de demonstração no `src/data/members.json`
5. Faça o deploy do site no Netlify
6. Depois que você possuir um domínio, coloque-o no também no `meta.json`.

[![Deploy to Netlify](https://www.netlify.com/img/deploy/button.svg)](https://app.netlify.com/start/deploy?repository=https://gitlab.com/umbra-cultura/liga-de-blogs-kit-inicial)

## Incorporação do Banner

Membros podem copiar um pedaço de código e incorporá-lo aos seus sites.
Ele renderiza um componente web (com os links expostos como redundância).

O site de índice irá produzir um código de incorporação com a URL e título da sua liga:

```html
<webring-banner>
  <p>Member of <a href="{{ your-url }}">{{ your-title }}</a></p>
  <a href="{{ your-url }}/prev">Previous</a>
  <a href="{{ your-url }}/random">Random</a>
  <a href="{{ your-url }}/next">Next</a>
</webring-banner>
<script async src="{{ your-url }}/embed.js" charset="utf-8"></script>
```

The design is up to the ring admins. It could look something like this:

![the banner widget](.gitlab/banner.png)

## Desenvolvimento Local

Para fazer a build do site localmente, execute estes comandos:

```shell
# clone o repository
git clone git@gitlab.com:umbra-cultura/liga-de-blogs-kit-inicial.git

# vá para o diretório de trabalho
cd liga-de-blogs-kit-inicial

# instale as dependências
yarn

# inicie um servidor de build local e o pipeline gulp
yarn start
```
